<?php

$lang['lets_encrypt_app_name'] = 'Let\'s Encrypt';
$lang['lets_encrypt_app_description'] = 'Let\'s Encrypt — це відкритий центр сертифікації, який надає безкоштовні сертифікати SSL.';
$lang['lets_encrypt_certificate'] = 'Сертифікат';
$lang['lets_encrypt_certificates'] = 'Сертифікати';
$lang['lets_encrypt_expires'] = 'Термін дії закінчується';
$lang['lets_encrypt_default_email_address'] = 'E-mail адреса за замовчуванням';
$lang['lets_encrypt_issued'] = 'Виданий';
$lang['lets_encrypt_domains'] = 'Домени';
$lang['lets_encrypt_primary_domain'] = 'Основний домен';
$lang['lets_encrypt_other_domains'] = 'Інші домени';
$lang['lets_encrypt_domain_list_invalid'] = 'Список доменів недійсний.';
$lang['lets_encrypt_certificate_created'] = 'Сертифікат створено';
$lang['lets_encrypt_certificate_created_help'] = 'Ваш сертифікат SSL створено! Деталі нижче.';
$lang['lets_encrypt_renew_failed'] = 'Certificate renewal check failed.';
$lang['lets_encrypt_renew_succeeded'] = 'Перевірка поновлення сертифіката пройшла успішно.';
$lang['lets_encrypt_renew_not_required'] = 'Поновлення сертифіката не потрібне - нічого не потрібно поновлювати.';
$lang['lets_encrypt_connection_refused_warning'] = 'Схоже, системі Let\'s Encrypt не вдалося підключитися до вашої системи для перевірки доменів. Будь ласка, прочитайте документацію, щоб отримати поради щодо усунення несправностей! Деталі помилки підключення наведено нижче.';
